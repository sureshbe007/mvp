package com.rs.mvpsample.utils;

import android.content.Context;
import android.widget.Toast;

import com.rs.mvpsample.MyApp;

public class CommonUtils {

    public static final String BASE_URL = "https://jsonplaceholder.typicode.com/";
    public static final String GET_USERS = "users/";

    public static void displayToast(String str)
    {
        Toast.makeText(MyApp.getInstance(),str,Toast.LENGTH_SHORT).show();
    }
}
