package com.rs.mvpsample.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.rs.mvpsample.R;

public class DialogUtils {

    public static ProgressDialog progressDialog;

    private DialogUtils() {
    }

    public static ProgressDialog showDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;

    }

    public static void hideDialog() {
        if (progressDialog != null)
            progressDialog.cancel();
    }
}
