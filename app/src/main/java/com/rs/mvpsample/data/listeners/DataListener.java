package com.rs.mvpsample.data.listeners;

import com.rs.mvpsample.data.remote.model.DataModel;

import java.util.List;

public interface DataListener {

    void onResponse(List<DataModel> dataModelList);

    void onError(String error);
}
