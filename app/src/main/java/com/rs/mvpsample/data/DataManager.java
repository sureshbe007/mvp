package com.rs.mvpsample.data;

import com.rs.mvpsample.data.listeners.DataListener;
import com.rs.mvpsample.data.local.PreferencesHelper;
import com.rs.mvpsample.data.remote.ApiHelper;
import com.rs.mvpsample.data.remote.model.DataModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataManager {

    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper apiHelper;

    @Inject
    public DataManager(PreferencesHelper mPreferencesHelper, ApiHelper apiHelper) {
        this.mPreferencesHelper = mPreferencesHelper;
        this.apiHelper = apiHelper;
    }

    public void getUserDataManager(final DataListener dataListener) {

        apiHelper.getDataModel(new DataListener() {
            @Override
            public void onResponse(List<DataModel> dataModelList) {
                dataListener.onResponse(dataModelList);
            }

            @Override
            public void onError(String error) {
                dataListener.onError(error);
            }
        });
    }

}
