package com.rs.mvpsample.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.rs.mvpsample.di.annotation.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesHelper {

    private final SharedPreferences mPref;
    private static final String PREF_FILE_NAME = "pref_user";

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }
}
