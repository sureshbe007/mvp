package com.rs.mvpsample.data.remote;

import com.rs.mvpsample.data.remote.model.DataModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

import static com.rs.mvpsample.utils.CommonUtils.GET_USERS;


public interface ApiInterface {

    @GET(GET_USERS)
    Call<List<DataModel>> getUsers();
}
