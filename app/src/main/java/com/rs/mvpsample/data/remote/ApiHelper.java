package com.rs.mvpsample.data.remote;

import android.util.Log;

import com.rs.mvpsample.data.listeners.DataListener;
import com.rs.mvpsample.data.remote.model.DataModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class ApiHelper {

    @Inject
    ApiInterface apiInterface;

    @Inject
    public ApiHelper() {
    }

    public void getDataModel(DataListener dataListener) {

        apiInterface.getUsers().enqueue(new Callback<List<DataModel>>() {
            @Override
            public void onResponse(Call<List<DataModel>> call, Response<List<DataModel>> response) {

                List<DataModel> dataModel = response.body();
                if (dataModel != null)
                    dataListener.onResponse(dataModel);
            }

            @Override
            public void onFailure(Call<List<DataModel>> call, Throwable t) {
                dataListener.onError("Network Error");
            }
        });
    }



}
