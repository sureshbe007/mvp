package com.rs.mvpsample.ui.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.Toast;

import com.rs.mvpsample.MyApp;
import com.rs.mvpsample.di.component.ActivityComponent;
import com.rs.mvpsample.di.component.DaggerActivityComponent;
import com.rs.mvpsample.di.module.ActivityModule;
import com.rs.mvpsample.ui.main.MainActivity;
import com.rs.mvpsample.utils.DialogUtils;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;

public  abstract class BaseActivity extends AppCompatActivity implements MvpView {

    private ActivityComponent mActivityComponent;
    private ProgressDialog progressDialog;


    public ActivityComponent activityComponent() {

        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(MyApp.getInstance(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }







    @Override
    public void showLoading() {
        hideLoading();
        progressDialog = DialogUtils.showDialog(this);
    }

    @Override
    public void hideLoading() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }

    }
}
