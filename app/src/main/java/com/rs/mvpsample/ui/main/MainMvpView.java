package com.rs.mvpsample.ui.main;

import com.rs.mvpsample.data.remote.model.DataModel;
import com.rs.mvpsample.ui.base.MvpView;

import java.util.List;

public interface MainMvpView extends MvpView {

    void showData(List<DataModel> dataModelList);
    void showError(String str);
}
