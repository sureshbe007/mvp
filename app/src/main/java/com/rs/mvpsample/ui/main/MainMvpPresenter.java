package com.rs.mvpsample.ui.main;

import com.rs.mvpsample.ui.base.MvpPresenter;

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

    void getUserData();
}
