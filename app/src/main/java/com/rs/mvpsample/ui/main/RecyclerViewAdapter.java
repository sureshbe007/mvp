package com.rs.mvpsample.ui.main;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rs.mvpsample.R;
import com.rs.mvpsample.data.remote.model.DataModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<DataModel> mModelList;
    private Context mContext;

    @Inject
    public RecyclerViewAdapter() {
        mModelList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        mContext = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        DataModel dataModel = mModelList.get(position);
        holder.textView.setText(dataModel.getUsername());
        holder.address1.setText(dataModel.getAddress().getStreet());
        holder.address2.setText(dataModel.getAddress().getSuite());
        holder.address3.setText(dataModel.getAddress().getCity());
        holder.address4.setText(dataModel.getAddress().getZipcode());
        holder.profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

            }
        });
        holder.emailImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"some@email.address"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "mail body");
                mContext.startActivity(Intent.createChooser(intent, ""));
//                MainActivity.clickListener("suresh");

            }
        });

        holder.geoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double lat= Location.convert(dataModel.getAddress().getGeo().getLat());
                double lang= Location.convert(dataModel.getAddress().getGeo().getLng());
                Intent mapIntent=new Intent(mContext, MapsActivity.class);
                mapIntent.putExtra("LAT",lat);
                mapIntent.putExtra("LANG",lang);
                mContext.startActivity(mapIntent);

            }
        });
//
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    public void setData(List<DataModel> dataModels) {
        this.mModelList.addAll(dataModels);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_name)
        public TextView textView;

        @BindView(R.id.text_address1)
        public TextView address1;
        @BindView(R.id.text_address2)
        public TextView address2;
        @BindView(R.id.text_address3)
        public TextView address3;
        @BindView(R.id.text_address4)
        public TextView address4;

        @BindView(R.id.image_profile)
        public ImageView profileImage;

        @BindView(R.id.image_geo)
        public ImageView geoImage;

        @BindView(R.id.image_email)
        public ImageView emailImage;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
