package com.rs.mvpsample.ui.base;

public interface MvpView {

    void showLoading();
    void hideLoading();
}
