package com.rs.mvpsample.ui.main;


import com.rs.mvpsample.data.DataManager;
import com.rs.mvpsample.data.listeners.DataListener;
import com.rs.mvpsample.data.remote.model.DataModel;
import com.rs.mvpsample.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    private DataManager mDataManager;

    @Inject
    public MainPresenter(DataManager dataManager) {
        this.mDataManager = dataManager;
    }

    @Override
    public void getUserData() {
        getView().showLoading();

        mDataManager.getUserDataManager(new DataListener() {
            @Override
            public void onResponse(List<DataModel> dataModelList) {

                getView().showData(dataModelList);
            }

            @Override
            public void onError(String error) {
                getView().showError(error);
            }
        });
    }
}
