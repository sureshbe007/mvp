package com.rs.mvpsample.di.component;

import android.app.Application;
import android.content.Context;

import com.rs.mvpsample.MyApp;
import com.rs.mvpsample.data.DataManager;
import com.rs.mvpsample.data.local.PreferencesHelper;
import com.rs.mvpsample.data.remote.ApiHelper;
import com.rs.mvpsample.data.remote.ApiInterface;
import com.rs.mvpsample.di.annotation.ApplicationContext;
import com.rs.mvpsample.di.module.ApplicationModule;
import com.rs.mvpsample.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void Inject(MyApp myApp);

    @ApplicationContext
    Context context();

    Application application();

    DataManager dataManager();

    PreferencesHelper preferencesHelper();

    ApiInterface  getApiInterface();
}
