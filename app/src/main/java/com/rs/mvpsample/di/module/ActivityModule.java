package com.rs.mvpsample.di.module;

import android.app.Activity;
import android.content.Context;

import com.rs.mvpsample.di.annotation.ActivityContext;
import com.rs.mvpsample.di.annotation.PerActivity;
import com.rs.mvpsample.ui.main.MainMvpPresenter;
import com.rs.mvpsample.ui.main.MainMvpView;
import com.rs.mvpsample.ui.main.MainPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }
}
