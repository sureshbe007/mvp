package com.rs.mvpsample.di.module;

import android.content.Context;

import com.rs.mvpsample.ui.main.MainActivity;
import com.rs.mvpsample.ui.main.RecyclerViewAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class AdapterModule {



    @Provides
    public RecyclerViewAdapter getDataModelAdapter() {
        return new RecyclerViewAdapter();
    }
}
