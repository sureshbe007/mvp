package com.rs.mvpsample.di.component;

import com.rs.mvpsample.di.annotation.PerActivity;
import com.rs.mvpsample.di.module.ActivityModule;
import com.rs.mvpsample.di.module.AdapterModule;
import com.rs.mvpsample.ui.main.MainActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = {ActivityModule.class, AdapterModule.class})
public interface ActivityComponent {

    void inject(MainActivity mainActivity);
}
