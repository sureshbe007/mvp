package com.rs.mvpsample;

import android.app.Application;
import android.content.Context;

import com.rs.mvpsample.di.component.ApplicationComponent;
import com.rs.mvpsample.di.component.DaggerApplicationComponent;
import com.rs.mvpsample.di.module.ApplicationModule;

public class MyApp extends Application {

    ApplicationComponent mApplicationComponent;
    private static MyApp instance;

    public static MyApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance=this;
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.Inject(this);
    }

    public static MyApp getInstance(Context context) {
        return (MyApp) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    public void setComponent(ApplicationComponent applicationComponent) {

        mApplicationComponent = applicationComponent;
    }
}
